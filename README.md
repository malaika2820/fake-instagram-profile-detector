## About the Project

Fake profiles are often found on social media platforms. 
Using properties like presence of profile pictures, number of posts and number of followers and follows, an artificial neural network in designed, to categorise fake and legitimate profiles. 

## Dataset 

The dataset is taken from _kaggle_. It can be found [here](https://www.kaggle.com/free4ever1/instagram-fake-spammer-genuine-accounts)

There are 576 entries in training dataset and 120 entries in testing dataset.
There are 11 features which help in determining whether a profile is fake or legitimate. They are -
1. profile pic	
2. length of username	
3. fullname	
4. length of fullname 
5. fullname	== username	
5. description length	
6. external URL	
7. private	
8. number of posts	
9. number of followers	
10. number follows	
11. fake










